import { add } from 'date-fns';

export let counter = 0;

export const addCount = (amount: number = 1) => {
  counter += amount;
  setTimeout(() => {
    counter -= amount;
  }, 1 * 1000);
};

export const waitUntilReady = (limit: number = 100) => {
  return new Promise((resolve) => {
    let announced = false;
    if (counter < limit) {
      addCount();
      resolve();
    } else {
      const interval = setInterval(() => {
        if (counter < limit) {
          clearInterval(interval);
          addCount();
          resolve();
        } else {
          if (!announced) {
            console.log(
              `[⏳] Menunggu rate limit berkurang, jumlah request 1 menit terakhir: ${counter}`
            );
            announced = true;
          }
        }
      }, 1000);
    }
  });
};
